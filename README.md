# Views Ajax Completed Event

## CONTENTS OF THIS FILE
* Introduction
* Installation

### INTRODUCTION
For a full description of the module, visit the project page:
https://drupal.org/project/views_ajax_completed_event

### INSTALLATION
To install the module and its dependencies, use Composer:
```
composer require drupal/views_ajax_completed_event
```
