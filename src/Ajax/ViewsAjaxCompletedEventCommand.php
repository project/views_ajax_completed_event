<?php

namespace Drupal\views_ajax_completed_event\Ajax;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Raise a Javascript event when views ajax is completed.
 */
class ViewsAjaxCompletedEventCommand implements CommandInterface {

  /**
   * The view ID.
   *
   * @var string
   */
  protected $viewName;

  /**
   * The view display ID.
   *
   * @var string
   */
  protected $viewDisplayId;

  /**
   * The view DOM ID.
   *
   * @var string
   */
  protected $viewDomId;

  public function __construct($view_name, $view_display_id, $view_dom_id) {
    $this->viewName = $view_name;
    $this->viewDisplayId = $view_display_id;
    $this->viewDomId = $view_dom_id;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'viewsAjaxCompletedEventCommand',
      'view_name' => $this->viewName,
      'view_display_id' => $this->viewDisplayId,
      'view_dom_id' => $this->viewDomId,
    ];
  }

}
