(Drupal => {
"use strict";
    
    Drupal.AjaxCommands.prototype.viewsAjaxCompletedEventCommand = (ajax, response) => {
        const data = {
            view_name: response.view_name,
            view_display_id: response.view_display_id,
            view_dom_id: response.view_dom_id,
            view_class: `js-view-dom-id-${response.view_dom_id}`,
        };
        // namespace the event so that it does not interfere with another event named "views_ajax_completed"
        const event = new CustomEvent('views_ajax_completed_event:views_ajax_completed', { detail: data });
        // dispatch event on the document object
        document.dispatchEvent(event);
    };
})(Drupal);
