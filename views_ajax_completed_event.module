<?php

/**
 * @file
 * Raise a Javascript event when views ajax has completed.
 */

use Drupal\views\ViewExecutable;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\views_ajax_completed_event\Ajax\ViewsAjaxCompletedEventCommand;

/**
 * Implements hook_ajax_render_alter().
 */
function views_ajax_completed_event_ajax_render_alter(array &$data) {
  $element = reset($data);
  if (isset($element['settings']['views']['ajaxViews'])) {
    $ajaxViews = reset($element['settings']['views']['ajaxViews']);
    if (isset($ajaxViews['view_name'], $ajaxViews['view_display_id'])) {
      $command = new ViewsAjaxCompletedEventCommand($ajaxViews['view_name'], $ajaxViews['view_display_id'], $ajaxViews['view_dom_id']);
      $data[] = $command->render();
    }
  }
}

/**
 * Implements hook_views_pre_render().
 */
function views_ajax_completed_event_views_pre_render(ViewExecutable $view) {
  if ($view->display_handler->usesAJAX()) {
    $view->element['#attached']['library'][] = 'views_ajax_completed_event/views-ajax-completed-event';
  }
}


/**
 * Implements hook_help().
 */
function views_ajax_completed_event_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.views_ajax_completed_event':
      $output = '';
      $output .= '<p>' . t('Provides for a Javascript event when Views ajax has completed. This module is useful for projects which do not use JQuery. If jQuery is being used then the <code>ajaxComplete</code> event should suffice.') . '</p>';
      $output .= '<h3>' . t('Features') . '</h3>';
      $output .= '<ul>';
      $output .= '<li>' . t('Raise the event <code>views_ajax_completed_event:views_ajax_completed</code> on the document object when Views ajax has completed.') . '</li>';
      $output .= '<li>' . t('The <code>view_name</code> and the <code>view_display_id</code> can be obtained from the <code>detail</code> property on the event object.') . '</li>';
      $output .= '</ul>';
      $output .= '<p><a href="https://www.drupal.org/project/views_ajax_completed_event" target="_blank">' . t('More information') . '</a></p>';
      return $output;
  }
}
